@extends('layouts.user')

@section('content')
@if(session()->has('success'))
<script>
    Swal.fire(
        'Peminjaman Buku Berhasil!',
        'Peminjaman buku maksimal 7 hari terhitung dari tanggal peminjaman',
        'success'
    )
</script>
@endif

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Form {{ $title }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('peminjaman-buku.store') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <select class="custom-select form-control-border" name="buku" id="buku">
                    <option value=""> Pilih Buku </option>
                    @foreach ($buku as $buku)
                        <option value="{{ $buku->id }}"> {{ $buku->judul }} </option>
                    @endforeach
                </select>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection