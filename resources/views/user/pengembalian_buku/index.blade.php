@extends('layouts.user')
@php
    $title = 'Pengembalian Buku'
@endphp

@section('content')
@if(session()->has('success'))
<script>
    Swal.fire(
        'Berhasil!',
        'Pengembalian Buku Berhasil!',
        'success'
    )
</script>
@endif
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content ">
    <div class="card card-primary " >
        <div class="card-header">
        <h3 class="card-title">Form {{ $title }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('pengembalian-buku.store') }}" method="post">
            @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="no_regis" class="col-form-label">{{ __('No. Registrasi') }}</label>

                <div class="col-md-12">
                    <input class="form-control{{ $errors->has('no_regis') ? ' is-invalid' : '' }}"  type="text" name="no_regis">  
                    @if ($errors->has('no_regis'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('no_regis') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection