@extends('layouts.user')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
<center>
<embed type="application/pdf" src="{{asset(Storage::url($path))}}" width="1000" height="400"></embed>
</center>
</section>
<!-- /.content -->
@endsection