@extends('layouts.user')
@php
    $title = 'Daftar Buku'
@endphp

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <div class="card mb-2 bg-gradient-dark">
                        <img class="card-img-top" src="{{ asset('storage/img/sample_buku.png') }}" alt="Dist Photo 1">
                        <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <a href="{{ route('bukuByKelas','10') }}">
                            <button class="btn btn-primary"> Buku Kelas 10 </button>
                        </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <div class="card mb-2 bg-gradient-dark">
                        <img class="card-img-top" src="{{ asset('storage/img/sample_buku.png') }}" alt="Dist Photo 1">
                        <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <a href="{{ route('bukuByKelas','11') }}">
                            <button class="btn btn-primary"> Buku Kelas 11 </button>
                        </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <div class="card mb-2 bg-gradient-dark">
                        <img class="card-img-top" src="{{ asset('storage/img/sample_buku.png') }}" alt="Dist Photo 1">
                        <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <a href="{{ route('bukuByKelas','12') }}">
                            <button class="btn btn-primary"> Buku Kelas 12 </button>
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection