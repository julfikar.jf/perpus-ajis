@extends('layouts.user')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-success">
        <div class="card-body">
            <div class="row">
                @foreach ($buku as $buku)
                    <div class="col-md-12 col-lg-6 col-xl-4">
                        <div class="card mb-2 bg-gradient-dark">
                            <img class="card-img-top" src="{{ asset('storage/img/sample_buku.png') }}" alt="Dist Photo 1">
                            <div class="card-img-overlay d-flex flex-column justify-content-end">
                            <h5 class="card-title text-primary text-white">{{ $buku->judul }}</h5>
                            <p class="card-text text-white pb-2 pt-1">{{ $buku->pengarang }}</p>
                            <a href="#" class="text-white">{{ $buku->tahun_terbit }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection