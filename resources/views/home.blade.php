@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Berhasil Login! Klik <a href="{{ Auth::user()->isAdmin == '0' ? route('dashboard') : route('admin.buku.index') }}">disini</a> untuk melanjutkan. 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
