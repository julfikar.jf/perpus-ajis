@extends('layouts.admin')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Peminjaman</h3>
                <!-- <a href=" {{ route('peminjaman-buku.create') }} "> <button class="btn btn-primary float-right">Pinjam</button> </a> -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>No. Regis</th>
                      <th>Judul Buku</th>
                      <th>Tanggal Peminjaman</th>
                      <th style="width: 40px">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $data)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $data->no_regis }}</td>
                            <td>{{ $data->judul }}</td>
                            <td>
                                {{ $data->tanggal_pinjam }}
                            </td>
                            <td>
                                {{ ($data->sudah_dikembalikan != '0') ? 'Sudah Dikembalikan' : 'Belum Dikembalikan' }}
                                
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection