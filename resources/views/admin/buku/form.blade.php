@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Form {{ $title }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form id="data" action="{{ route('buku.store') }}" method="POST" enctype= "multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul Buku</label>
                <input class="form-control" type="text" name="judul" id="judul">
            </div>
            <div class="form-group">
                <label for="pengarang">Pengarang</label>
                <input class="form-control" type="text" name="pengarang" id="pengarang">
            </div>
            <div class="form-group">
                <label for="tahun_terbit">Tahun Terbit</label>
                <input class="form-control" type="number" name="tahun_terbit" id="tahun_terbit">
            </div>
            <div class="form-group">
                <select class="custom-select form-control-border" name="kelas" id="kelas">
                    <option value=""> Pilih Kelas </option>
                    <option value="10">Kelas 10</option>
                    <option value="11">Kelas 11</option>
                    <option value="12">Kelas 12</option>
                </select>
            </div>
            <div class="form-group">
                <label for="foto">Foto Cover</label>
                <input class="form-control" type="file" name="foto" id="foto">
            </div>
            <div class="form-group">
                <label for="pdf">File PDF</label>
                <input class="form-control" type="file" name="pdf" id="pdf">
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button id="btn-submit" type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection

@section('script')

    <!-- <script>
        $('form#data').submit(function(event){
            event.preventDefault();
            var form = $('form#data'),
                url = form.attr('action'),
                method = form.attr('method');
            var formData = new FormData(this);
            console.log(form.serialize)

                
            var judul = document.getElementById("judul").value;
            var pengarang = document.getElementById("pengarang").value;
            var tahun_terbit = document.getElementById("tahun_terbit").value;
            var kelas = document.getElementById("kelas").value;

            var url = "{{ route('buku.store') }}";
            var method = "POST";
            swal({
                title: 'Konfirmasi',
                text: 'Yakin Data Sudah benar ?',
                icon: 'warning',
                buttons : {
                    cancel : {
                        text: 'Tidak',
                        value: null,
                        visible: true,
                        className: "btn btn-default btn-waves"
                    },
                    confirm : {
                        text: 'Ya',
                        value: true,
                        className: "btn btn-info btn-waves",
                    }
                }
            }).then((value) => {
                if(value)
                {
                    $.ajax({
                        url : url,
                        method : method,
                        data : formData,
                        rocessData : false,
                        contentType: false,
                        success : function(response){
                            console.log(response)
                            
                            swal({
                                title: response.title,
                                text: response.msg,
                                icon: response.status
                            })
                        },
                        error: function(xhr){
                            var res = xhr.responseJSON;
                            if($.isEmptyObject(res) == false){
                                $.each(res.errors, function(key, value){
                                    $('#' + key)
                                    .closest('.form-group')
                                    .addClass('has-error')
                                    .append('<span class="help-block"><strong>'+ value +'</strong></span>')
                                })
                            }
                        }
                    });
                }
            })
        });
    </script> -->
        
@endsection