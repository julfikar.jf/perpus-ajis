@extends('layouts.admin')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Peminjaman</h3>
                <!-- <a href=" {{ route('peminjaman-buku.create') }} "> <button class="btn btn-primary float-right">Pinjam</button> </a> -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>No. Regis</th>
                      <th>Judul Buku</th>
                      <th>Tanggal Peminjaman</th>
                      <th style="width: 40px">Bukti Pembayaran</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $data)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $data->no_regis }}</td>
                            <td>{{ $data->judul }}</td>
                            <td>
                                {{ $data->jml_bayar }}
                            </td>
                            <td>
                            <button onClick="fileBukti('{{ $data->file_bukti }}')" type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                                Lihat File Bukti
                            </button>
                                <!-- {{ $data->file_bukti }} -->
                                
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">File Bukti </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img id="fileBukti" src="" alt="file_bukti" width="100%" height="auto">
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
<!-- /.content -->
@endsection

@section('script')
    <script>
        function fileBukti(path){
            var src = "{{asset(Storage::url('path'))}}"
            src = src.replace('path',path)
            src = src.replace('public/',"")
            console.log(src)
            $('#fileBukti').attr('src',src)
        }
    </script>
@endsection