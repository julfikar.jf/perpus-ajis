@extends('layouts.admin')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $title }}</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content ">
    <div class="card card-primary " >
        <div class="card-header">
        <h3 class="card-title">Form {{ $title }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-form">
        <form action="{{ route('denda-pengembalian.hitung') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="no_regis" class="col-form-label">{{ __('No. Registrasi') }}</label>
                    <div class="col-md-12">
                        <input class="form-control{{ $errors->has('no_regis') ? ' is-invalid' : '' }}"  type="text" name="no_regis" id="no_regis">  
                        @if ($errors->has('no_regis'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('no_regis') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button id="btn-submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
            
    </div>
</section>
<!-- /.content -->
@endsection

@section('script')
    <script>
        $('#btn-submit').click(function(event){
            console.log(1)
            event.preventDefault();
            var data = document.getElementById("no_regis").value;

            var url = "{{ route('denda-pengembalian.hitung') }}";
            var method = "POST";
                console.log(url,method)

            swal({
                title: 'Konfirmasi',
                text: 'Yakin Data Sudah benar ?',
                icon: 'warning',
                buttons : {
                    cancel : {
                        text: 'Tidak',
                        value: null,
                        visible: true,
                        className: "btn btn-default btn-waves"
                    },
                    confirm : {
                        text: 'Ya',
                        value: true,
                        className: "btn btn-info btn-waves",
                    }
                }
            }).then((value) => {
                if(value)
                {
                    $.ajax({
                        url : url,
                        method : method,
                        data : {no_regis:data},
                        success : function(response){
                            console.log(response)
                            
                            swal({
                                title: response.title,
                                text: response.msg,
                                icon: response.status
                            })
                        },
                        error: function(xhr){
                            var res = xhr.responseJSON;
                            if($.isEmptyObject(res) == false){
                                $.each(res.errors, function(key, value){
                                    $('#' + key)
                                    .closest('.form-group')
                                    .addClass('has-error')
                                    .append('<span class="help-block"><strong>'+ value +'</strong></span>')
                                })
                            }
                        }
                    });
                }
            })
        });
    </script>
@endsection