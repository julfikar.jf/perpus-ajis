<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 10; $i++){
            DB::table('buku')->insert([
                'judul' => $faker->sentence(),
                'pengarang' => $faker->name(),
                'tahun_terbit' => $faker->year(),
                'cover_path' => "img/sample_buku.png",
                'kelas' => "".mt_rand(10,12).""
            ]);
        }
    }
}
