<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = [
        'judul',
        'pengarang',
        'tahun_terbit',
        'kelas',
        'cover_path',
        'pdf_path'
    ];
}
