<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeminjamanBuku extends Model
{
    protected $table = "peminjaman";
    protected $fillable = [
        'user_id',
        'buku_id',
        'no_regis',
        'status'
    ];

}
