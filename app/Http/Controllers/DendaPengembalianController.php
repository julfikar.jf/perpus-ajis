<?php

namespace App\Http\Controllers;

use App\PeminjamanBuku;
use \Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DendaPengembalianController extends Controller
{
    public function index()
    {
        return view('user.denda_pengembalian.index');
    }

    public function cek_denda(Request $request)
    {
        $msg = "";
        $today = Carbon::now();
        $data = PeminjamanBuku::where('no_regis',$request->no_regis)->first();
        if(!$data){
            return [
                'title' => "Error",
                'status' => "error",
                'msg' => "Data yang anda masukan tidak ada"
            ];
        }
        $tgl_pinjam = $data->created_at;
        $tgl_kembali = strtotime("+7 day", strtotime($tgl_pinjam));
        $tgl_kembali = date('Y-m-d', $tgl_kembali);


        $t = date_create($tgl_kembali);
        $n = date_create(date('Y-m-d'));
        $terlambat = date_diff($t, $n);
        $hari = $terlambat->format("%a");
        $denda = $hari * 5000;


        if($today > $tgl_kembali){
            $msg = "Telat ". $hari . " hari. \nDenda yang harus dibayar adalah Rp. " . $denda;
            return [
                'title' => "Success",
                'status' => "success",
                'msg' => $msg
            ];
        }else{
            return [
                'title' => "Success",
                'status' => "success",
                'msg' => "Tidak ada denda, waktu peminjaman belum melebihi waktu pengembalian."
            ];
        }
    }
}
