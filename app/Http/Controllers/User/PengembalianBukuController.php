<?php

namespace App\Http\Controllers\User;

use App\PeminjamanBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PengembalianBukuController extends Controller
{
    public function index()
    {
        return view('user.pengembalian_buku.index');
    }

    public function store(Request $request)
    {
        
        $ubahStatus = PeminjamanBuku::where('no_regis',$request->no_regis)->update([
            'sudah_dikembalikan' => '1'
        ]);

        if($ubahStatus){
            return redirect()->back()->with('success','pengembalian');  
        }
    }
}
