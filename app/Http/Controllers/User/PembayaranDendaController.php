<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PembayaranDenda;
use App\PeminjamanBuku;
use Storage;

class PembayaranDendaController extends Controller
{
    public function index()
    {
        return view('user.bayar_denda.index',[
            'title'     => "Pembayaran Denda"
        ]);
    }

    public function bayar(Request $request)
    {
        // return $request->all();
        $data = PeminjamanBuku::where('no_regis', $request->no_regis)->firstOrFail();
        

        //awal block code untuk hitung denda
        $tgl_pinjam = $data->created_at;
        $tgl_kembali = strtotime("+7 day", strtotime($tgl_pinjam));
        $tgl_kembali = date('Y-m-d', $tgl_kembali);

        $t = date_create($tgl_kembali);
        $n = date_create(date('Y-m-d'));
        //mencari selisih hari
        $terlambat = date_diff($t, $n);
        $hari = $terlambat->format("%a");
        //menghitung selisih hari di kali denda(5000)
        $denda = $hari * 5000;
        //akhir block code untuk hitung denda

        //awal block code untuk menyimpan file bukti
        if($request->file('foto')){
            $foto = $request->file('foto');
            $path_file_bukti = Storage::putFile(
                'public/img/file_bukti',
                $foto
            );
        }
        //akhir block code untuk menyimpan file bukti

        $bayar = PembayaranDenda::create([
            'no_regis'      => $data->no_regis,
            'jml_bayar'     => $denda,
            'file_bukti'    => $path_file_bukti
        ]);

        //update status peminjaman
        $update = PeminjamanBuku::where('no_regis',$request->no_regis)->update([
            'sudah_dikembalikan' => '1'
        ]); 
        

        if($bayar){
            return redirect()->back()->with('success','bayar-denda');
        }
    }
}
