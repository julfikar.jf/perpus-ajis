<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Buku;

class DashboardController extends Controller
{
    public function index()
    {
        $buku = Buku::orderBy('created_at','ASC')->limit(3)->get();
        return view('user.dashboard',[
            'buku' => $buku,
            'title' => "Dashboard"
        ]);
    }
}
