<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Buku;

class DaftarBukuController extends Controller
{
    public function index()
    {
        return view('user.daftar_buku.index');
    }

    public function bukuByKelas($kelas)
    {
        $buku = Buku::where('kelas',$kelas)->get();
        return view('user.daftar_buku.kelas',[
            'buku' => $buku,
            'title' => "Buku Kelas ".$kelas
        ]);
    }

    public function baca($id)
    {
        $data = Buku::findOrFail($id);
        return view('user.daftar_buku.baca',[
            'path' => $data->pdf_path,
            'title' => $data->judul
        ]);
    }
    
}
