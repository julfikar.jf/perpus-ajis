<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Buku;
use App\PeminjamanBuku;
use Auth;

use SweetAlert;

class PeminjamanBukuController extends Controller
{
    public function index()
    {
        $peminjaman = PeminjamanBuku::join('buku','buku.id','=','peminjaman.buku_id')
                        ->where('user_id', Auth::user()->id)
                        ->select('peminjaman.no_regis','buku.judul','peminjaman.created_at as tanggal_pinjam','peminjaman.sudah_dikembalikan')
                        ->get();
        // return $peminjaman;
        return view('user.peminjaman_buku.index',[
            'data' => $peminjaman,
            'title' => 'Data Peminjaman Buku',
            'no' => 1
        ]);
    }

    public function create()
    {
        $buku = Buku::get();

        return view('user.peminjaman_buku.form',[
            'buku' => $buku,
            'title' => 'Peminjaman Buku'
        ]);
    }

    public function store(Request $request)
    {
        $store = PeminjamanBuku::create([
            'user_id' => Auth::user()->id,
            'buku_id' => $request->buku,
            'no_regis' => mt_rand(1000000,9999999) 
        ]);

        if($store){
            return redirect()->route('peminjaman-buku')->with('success', 'peminjaman');  
        }
    }
}
