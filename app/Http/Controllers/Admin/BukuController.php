<?php

namespace App\Http\Controllers\Admin;

use App\Buku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BukuController extends Controller
{
    public function index()
    {
        $buku = Buku::get();
        return view('admin.buku.index',[
            'buku' => $buku,
            'title' => 'Data Buku',
            'no' => 1
        ]);
    }

    public function create()
    {
        return view('admin.buku.form',[
            'title' => 'Input Data Buku'
        ]);
    }

    public function store(Request $request)
    {
        if($request->file('foto')){
            $foto = $request->file('foto');
            $path_foto_cover = Storage::putFile(
                'public/img',
                $foto
            );
        }

        if($request->file('pdf')){
            $pdf = $request->file('pdf');
            $path_pdf_cover = Storage::putFile(
                'public/pdf',
                $pdf
            );
        }

        $store = Buku::create([
            'judul' => $request->judul,
            'pengarang' => $request->pengarang,
            'tahun_terbit' => $request->tahun_terbit,
            'kelas' => $request->kelas,
            'cover_path' => $path_foto_cover, //"img/sample_buku.png"
            'pdf_path' => $path_pdf_cover
        ]);

        if($store){
            return redirect()->back()->with('success','Berhasil menambahkan data');
        }
        // if ($store){
        //     return [
        //         'title' => "Success",
        //         'status' => "success",
        //         'msg' => "Data Berhasil Diinput"
        //     ];
        // }else{
        //     return [
        //         'title' => "Error",
        //         'status' => "error",
        //         'msg' => "Terjadi kesalahan"
        //     ];
        // }
    }
}
