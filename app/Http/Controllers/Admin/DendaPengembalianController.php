<?php

namespace App\Http\Controllers\Admin;

use App\PeminjamanBuku;
use App\PembayaranDenda;
use Illuminate\Http\Request;
use \Carbon\Carbon as Carbon;
use App\Http\Controllers\Controller;

class DendaPengembalianController extends Controller
{
    public function index()
    {
        return view('admin.denda_pengembalian.index',[
            'title' => "Cek Denda Buku"
        ]);
    }

    public function cek_denda(Request $request)
    {
        $msg = "";
        $today = Carbon::now();
        $data = PeminjamanBuku::where('no_regis',$request->no_regis)->first();
        if(!$data){
            return [
                'title' => "Error",
                'status' => "error",
                'msg' => "Data yang anda masukan tidak ada"
            ];
        }
        $tgl_pinjam = $data->created_at;
        $tgl_kembali = strtotime("+7 day", strtotime($tgl_pinjam));
        $tgl_kembali = date('Y-m-d', $tgl_kembali);


        $t = date_create($tgl_kembali);
        $n = date_create(date('Y-m-d'));
        $terlambat = date_diff($t, $n);
        $hari = $terlambat->format("%a");
        $denda = $hari * 5000;


        if($today > $tgl_kembali){
            $msg = "Telat ". $hari . " hari. \nDenda yang harus dibayar adalah Rp. " . $denda;
            return [
                'title' => "Success",
                'status' => "success",
                'msg' => $msg
            ];
        }else{
            return [
                'title' => "Success",
                'status' => "success",
                'msg' => "Tidak ada denda, waktu peminjaman belum melebihi waktu pengembalian."
            ];
        }
    }

    public function list()
    {
        // $data = PembayaranDenda::all();
        // return $data;
        $data = PembayaranDenda::select('peminjaman.no_regis','buku.judul','pembayaran_denda.jml_bayar','pembayaran_denda.file_bukti')
                                ->join('peminjaman','peminjaman.no_regis','=','pembayaran_denda.no_regis')
                                ->join('buku','buku.id','=','peminjaman.buku_id')
                                ->get();
        return view('admin.denda_pengembalian.list',[
            'data'  => $data,
            'title' => "List Denda Pengembalian Buku",
            'no'    => 1
        ]);
    }
}
