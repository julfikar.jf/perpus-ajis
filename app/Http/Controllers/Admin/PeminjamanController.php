<?php

namespace App\Http\Controllers\Admin;

use App\PeminjamanBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PeminjamanController extends Controller
{
    public function index()
    {
        $peminjaman = PeminjamanBuku::join('buku','buku.id','=','peminjaman.buku_id')
                        ->select('peminjaman.no_regis','buku.judul','peminjaman.created_at as tanggal_pinjam','peminjaman.sudah_dikembalikan')
                        ->get();
        // return $peminjaman;
        return view('admin.peminjaman.index',[
            'data' => $peminjaman,
            'title' => 'Data Peminjaman Buku',
            'no' => 1
        ]);
    }
}
