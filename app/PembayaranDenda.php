<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranDenda extends Model
{
    protected $table = 'pembayaran_denda';
    protected $fillable = [
        'id',
        'no_regis',
        'jml_bayar',
        'file_bukti'
    ];
}
