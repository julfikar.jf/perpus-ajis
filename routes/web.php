<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard','User\DashboardController@index')->name('dashboard');
Route::get('/daftar-buku','User\DaftarBukuController@index')->name('daftar-buku');
Route::get('/daftar-buku/{kelas}','User\DaftarBukuController@bukuByKelas')->name('bukuByKelas');
Route::get('/buku/baca/{id}','User\DaftarBukuController@baca')->name('baca');

Route::prefix('peminjaman-buku')->group(function () {    
    Route::get('/','User\PeminjamanBukuController@index')->name('peminjaman-buku');
    Route::get('/create','User\PeminjamanBukuController@create')->name('peminjaman-buku.create');
    Route::post('/store','User\PeminjamanBukuController@store')->name('peminjaman-buku.store');
});


Route::get('/pengembalian-buku','User\PengembalianBukuController@index')->name('pengembalian-buku');
Route::post('/pengembalian-buku/store','User\PengembalianBukuController@store')->name('pengembalian-buku.store');

Route::get('/denda-pengembalian/','DendaPengembalianController@index')->name('denda-pengembalian.index');
Route::get('/bayar-denda/','User\PembayaranDendaController@index')->name('bayar-denda.index');
Route::post('/bayar-denda/','User\PembayaranDendaController@bayar')->name('bayar-denda.bayar');

Route::group(['middleware' => 'isAdmin'], function(){   
    Route::prefix('admin')->group(function () {
        Route::prefix('buku')->group(function () {
            Route::get('/','Admin\BukuController@index')->name('admin.buku.index');
            Route::get('/create','Admin\BukuController@create')->name('admin.buku.create');
        });
        Route::prefix('peminjaman')->group(function () {
            Route::get('/','Admin\PeminjamanController@index')->name('admin.peminjaman');
        });
        Route::prefix('denda')->group(function () {
            Route::get('/','Admin\DendaPengembalianController@index')->name('admin.denda-pengembalian.index');
            Route::get('/list','Admin\DendaPengembalianController@list')->name('admin.denda-pengembalian.list');
        });
    });
});
